let icons = Ext.create('Ext.data.Store', {
    fields: [{
        'name': 'label',
        'type': 'string'
    }, {
        'name': 'className',
        'type': 'string'
    }],
    data: [
        {
            className: "fa fa-address-book",
            label: "address book"
        },
        {
            className: "fa fa-address-card",
            label: "address card"
        },
        {
            className: "fa fa-bell",
            label: "bell"
        },
        {
            className: "fa fa-bell-slash",
            label: "bell slash"
        },
        {
            className: "fa fa-bookmark",
            label: "bookmark"
        },
        {
            className: "fa fa-building",
            label: "building"
        },
        {
            className: "fa fa-calendar",
            label: "calendar"
        },
        {
            className: "fa fa-check-circle",
            label: "check circle"
        },
        {
            className: "fa fa-check-square",
            label: "check square"
        },
        {
            className: "fa fa-circle",
            label: "circle"
        },
        {
            className: "fa fa-clipboard",
            label: "clipboard"
        },
        {
            className: "fa fa-clone",
            label: "clone"
        },
        {
            className: "fa fa-comment",
            label: "comment"
        },
        {
            className: "fa fa-comments",
            label: "comments"
        },
        {
            className: "fa fa-compass",
            label: "compass"
        },
        {
            className: "fa fa-copy",
            label: "copy"
        },
        {
            className: "fa fa-copyright",
            label: "copyright"
        },
        {
            className: "fa fa-credit-card",
            label: "credit card"
        },
        {
            className: "fa fa-edit",
            label: "edit"
        },
        {
            className: "fa fa-envelope",
            label: "envelope"
        },
        {
            className: "fa fa-envelope-open",
            label: "envelope open"
        },
        {
            className: "fa fa-eye-slash",
            label: "eye slash"
        },
        {
            className: "fa fa-file",
            label: "file"
        },
        {
            className: "fa fa-flag",
            label: "flag"
        },
        {
            className: "fa fa-folder",
            label: "folder"
        },
        {
            className: "fa fa-folder-open",
            label: "folder open"
        },
        {
            className: "fa fa-heart",
            label: "heart"
        },
        {
            className: "fa fa-hourglass",
            label: "hourglass"
        },
        {
            className: "fa fa-id-badge",
            label: "id badge"
        },
        {
            className: "fa fa-id-card",
            label: "id card"
        },
        {
            className: "fa fa-image",
            label: "image"
        },
        {
            className: "fa fa-life-ring",
            label: "life ring"
        },
        {
            className: "fa fa-list-alt",
            label: "list alt"
        },
        {
            className: "fa fa-map",
            label: "map"
        },
        {
            className: "fa fa-minus-square",
            label: "minus square"
        },
        {
            className: "fa fa-object-group",
            label: "object group"
        },
        {
            className: "fa fa-object-ungroup",
            label: "object ungroup"
        },
        {
            className: "fa fa-paper-plane",
            label: "paper plane"
        },
        {
            className: "fa fa-pause-circle",
            label: "pause circle"
        },
        {
            className: "fa fa-play-circle",
            label: "play circle"
        },
        {
            className: "fa fa-plus-square",
            label: "plus square"
        },
        {
            className: "fa fa-question-circle",
            label: "question circle"
        },
        {
            className: "fa fa-registered",
            label: "registered"
        },
        {
            className: "fa fa-save",
            label: "save"
        },
        {
            className: "fa fa-share-square",
            label: "share square"
        },
        {
            className: "fa fa-square",
            label: "square"
        },
        {
            className: "fa fa-star",
            label: "star"
        },
        {
            className: "fa fa-star-half",
            label: "star half"
        },
        {
            className: "fa fa-sticky-note",
            label: "sticky note"
        },
        {
            className: "fa fa-stop-circle",
            label: "stop circle"
        },
        {
            className: "fa fa-thumbs-down",
            label: "thumbs down"
        },
        {
            className: "fa fa-thumbs-up",
            label: "thumbs up"
        },
        {
            className: "fa fa-times-circle",
            label: "times circle"
        },
        {
            className: "fa fa-user",
            label: "user"
        },
        {
            className: "fa fa-user-circle",
            label: "user circle"
        },
        {
            className: "fa fa-window-close",
            label: "window close"
        },
        {
            className: "fa fa-window-maximize",
            label: "window maximize"
        },
        {
            className: "fa fa-window-minimize",
            label: "window minimize"
        },
        {
            className: "fa fa-window-restore",
            label: "window restore"
        },
        {
            className: "fa fa-address-book",
            label: "address book"
        },
        {
            className: "fa fa-address-card",
            label: "address card"
        },
        {
            className: "fa fa-adjust",
            label: "adjust"
        },
        {
            className: "fa fa-align-center",
            label: "align center"
        },
        {
            className: "fa fa-align-justify",
            label: "align justify"
        },
        {
            className: "fa fa-align-left",
            label: "align left"
        },
        {
            className: "fa fa-align-right",
            label: "align right"
        },
        {
            className: "fa fa-ambulance",
            label: "ambulance"
        },
        {
            className: "fa fa-american-sign-language-interpreting",
            label: "american sign language interpreting"
        },
        {
            className: "fa fa-anchor",
            label: "anchor"
        },
        {
            className: "fa fa-angle-double-down",
            label: "angle double down"
        },
        {
            className: "fa fa-angle-double-left",
            label: "angle double left"
        },
        {
            className: "fa fa-angle-double-right",
            label: "angle double right"
        },
        {
            className: "fa fa-angle-double-up",
            label: "angle double up"
        },
        {
            className: "fa fa-angle-down",
            label: "angle down"
        },
        {
            className: "fa fa-angle-left",
            label: "angle left"
        },
        {
            className: "fa fa-angle-right",
            label: "angle right"
        },
        {
            className: "fa fa-angle-up",
            label: "angle up"
        },
        {
            className: "fa fa-archive",
            label: "archive"
        },
        {
            className: "fa fa-arrow-circle-down",
            label: "arrow circle down"
        },
        {
            className: "fa fa-arrow-circle-left",
            label: "arrow circle left"
        },
        {
            className: "fa fa-arrow-circle-right",
            label: "arrow circle right"
        },
        {
            className: "fa fa-arrow-circle-up",
            label: "arrow circle up"
        },
        {
            className: "fa fa-arrow-down",
            label: "arrow down"
        },
        {
            className: "fa fa-arrow-left",
            label: "arrow left"
        },
        {
            className: "fa fa-arrow-right",
            label: "arrow right"
        },
        {
            className: "fa fa-arrow-up",
            label: "arrow up"
        },
        {
            className: "fa fa-arrows-alt",
            label: "arrows alt"
        },
        {
            className: "fa fa-assistive-listening-systems",
            label: "assistive listening systems"
        },
        {
            className: "fa fa-asterisk",
            label: "asterisk"
        },
        {
            className: "fa fa-at",
            label: "at"
        },
        {
            className: "fa fa-audio-description",
            label: "audio description"
        },
        {
            className: "fa fa-backward",
            label: "backward"
        },
        {
            className: "fa fa-balance-scale",
            label: "balance scale"
        },
        {
            className: "fa fa-ban",
            label: "ban"
        },
        {
            className: "fa fa-barcode",
            label: "barcode"
        },
        {
            className: "fa fa-bars",
            label: "bars"
        },
        {
            className: "fa fa-bath",
            label: "bath"
        },
        {
            className: "fa fa-battery-empty",
            label: "battery empty"
        },
        {
            className: "fa fa-battery-full",
            label: "battery full"
        },
        {
            className: "fa fa-battery-half",
            label: "battery half"
        },
        {
            className: "fa fa-battery-quarter",
            label: "battery quarter"
        },
        {
            className: "fa fa-battery-three-quarters",
            label: "battery three quarters"
        },
        {
            className: "fa fa-bed",
            label: "bed"
        },
        {
            className: "fa fa-beer",
            label: "beer"
        },
        {
            className: "fa fa-bell",
            label: "bell"
        },
        {
            className: "fa fa-bell-slash",
            label: "bell slash"
        },
        {
            className: "fa fa-bicycle",
            label: "bicycle"
        },
        {
            className: "fa fa-binoculars",
            label: "binoculars"
        },
        {
            className: "fa fa-birthday-cake",
            label: "birthday cake"
        },
        {
            className: "fa fa-blind",
            label: "blind"
        },
        {
            className: "fa fa-bold",
            label: "bold"
        },
        {
            className: "fa fa-bolt",
            label: "bolt"
        },
        {
            className: "fa fa-bomb",
            label: "bomb"
        },
        {
            className: "fa fa-book",
            label: "book"
        },
        {
            className: "fa fa-bookmark",
            label: "bookmark"
        },
        {
            className: "fa fa-braille",
            label: "braille"
        },
        {
            className: "fa fa-briefcase",
            label: "briefcase"
        },
        {
            className: "fa fa-bug",
            label: "bug"
        },
        {
            className: "fa fa-building",
            label: "building"
        },
        {
            className: "fa fa-bullhorn",
            label: "bullhorn"
        },
        {
            className: "fa fa-bullseye",
            label: "bullseye"
        },
        {
            className: "fa fa-bus",
            label: "bus"
        },
        {
            className: "fa fa-calculator",
            label: "calculator"
        },
        {
            className: "fa fa-calendar",
            label: "calendar"
        },
        {
            className: "fa fa-camera",
            label: "camera"
        },
        {
            className: "fa fa-camera-retro",
            label: "camera retro"
        }
    ]
});

Ext.define('Ext.custom.icon.combobox', {
    extend: 'Ext.field.ComboBox',
    xtype: 'icon-combobox',
    store: icons,
    cls: 'icon-combobox',

    editable: true,
    queryMode: 'local',
    multiSelect: false,
    typeAhead: true,
    selectOnFocus: false,
    valueField: 'className',
    displayField: 'label',
    renderTo: Ext.getBody(),

    itemTpl: `<div style="font-size:150%; padding: 5px;">
                <i class="{className}"></i><span style="padding-left: 10px;">{label}</span>
              </div>`,

    listeners: {
        select: function (cmb, record) {
            let data = record.getData();
            let me = this;
            let imageElem = `<i style="font-size: 14px;" class="${data.className}"></i>`;

            if (me.labelElement) {
                me.labelElement.dom.innerHTML = `<span>Icon: </span>${imageElem}`;
            }

            if (me.beforeInputElement) {
                me.beforeInputElement.dom.innerHTML = imageElem;
            }
        }
    }
});
