Ext.define('AceEditorTextArea', {
    extend: 'Ext.Component',
    alias: 'widget.AceEditorTextArea',
    xtype: 'ace-editor',
    cls: 'prettyprint',

    listeners: {
        painted: function () {
            this.initAceEditor();
        }
    },

    defaultAceEditorOptions: {
        highlightActiveLine: true,
        showPrintMargin: false,
        showLineNumbers: false,
        mode: 'ace/mode/sql'
    },

    aceOptions: {},
    value: '',

    minHeight: 450,
    allowBlank: true,

    getAceEditor: function () {
        let me = this;
        if (!this.aceEditor) {
            this.aceEditor = ace.edit(this.element.id);
        }
        return this.aceEditor;
    },

    getValue: function () {
        return this.value;
    },

    initAceEditor: function () {
        let aceOptions = Ext.Object.merge(
            this.defaultAceEditorOptions,
            this.aceOptions
        );

        let aceEditor = this.getAceEditor();
        aceEditor.setOptions(aceOptions);
        aceEditor.getSession().setValue(js_beautify(this.getValue()));
        aceEditor.getSession().setMode(aceOptions.mode);
    }

});
