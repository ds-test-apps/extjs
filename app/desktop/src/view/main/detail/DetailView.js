Ext.define('ext-js-test-task.view.main.detail.DetailView', {
    extend: 'Ext.Container',
    xtype: 'detailview',
    cls: 'detailview',
    layout: 'fit'
});
