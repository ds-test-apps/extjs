Ext.define('ext-js-test-task.view.main.center.CenterView', {
    extend: 'Ext.Container',
    xtype: 'centerview',
    cls: 'centerview',
    layout: 'card'
});
