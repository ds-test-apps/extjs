Ext.define('ext-js-test-task.view.main.nav.top.TopView', {
    extend: 'Ext.Toolbar',
    xtype: 'topview',
    cls: 'topview',
    shadow: false,
    items: [
        {
            xtype: 'container',
            cls: 'topviewtext',
            bind: {
                html: '{name}',
                hidden: '{navCollapsed}'
            }
        }
    ]
});
