Ext.define('ext-js-test-task.view.home.IconComboboxView', {
    extend: 'Ext.Container',
    xtype: 'icon-combobox-view',
    cls: 'icon-combobox-view',
    controller: {type: 'iconComboboxViewController'},
    viewModel: {type: 'iconComboboxViewModel'},
    requires: [],
    scrollable: true,
    items: [
        Ext.create('Ext.panel.Panel', {
            bodyPadding: 5,
            width: 600,
            items: [{
                xtype: 'icon-combobox'
            }]
        })
    ]
});
