Ext.define('ext-js-test-task.view.home.IconComboboxModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.iconComboboxViewModel',
    data: {
        name: 'icon-combobox-view'
    }
});
