Ext.define('ext-js-test-task.Application', {
    extend: 'Ext.app.Application',
    name: 'ext-js-test-task',
    requires: ['ext-js-test-task.*'],
    defaultToken: 'icon-combobox-view',

    removeSplash: function () {
        Ext.getBody().removeCls('launching');
        var elem = document.getElementById("splash");
        elem.parentNode.removeChild(elem)
    },

    launch: function () {
        this.removeSplash();
        var whichView = 'mainview';
        Ext.Viewport.add([{xtype: whichView}]);
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
